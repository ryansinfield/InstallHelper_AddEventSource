﻿using System;
using System.Diagnostics;

namespace InstallHelper_AddEventSource
{
    class Program
    {
        static void Main(string[] args)
        {
            string source = args[0];
            string log = args[1];
            EventLog eLog = new EventLog();

            // Create the Dyn2Bib Event log if it does not already exist
            if (!EventLog.SourceExists(source))
            {
                EventLog.CreateEventSource(source, log);
                eLog.Source = source;
                eLog.WriteEntry($"{source} event source created.");
            }

            // Write to the event source
            eLog.Source = source;
            eLog.WriteEntry($"{source} event source working.");
        }
    }
}
